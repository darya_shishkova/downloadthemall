package ru.dashko.downloadthemall;

import java.io.*;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

/**
 * Поток, выполняющий скачивание музыкальных файлов
 *
 * @author Дарья "dASHKO" Шишкова, 16ИТ18к
 */
public class DownloadMusicThread extends Thread {
    private String linksFile;
    private String pathToMusic;

    /**
     * Конструктор для потока скачивания
     *
     * @param linksFile   путь к файлу с сылками
     * @param pathToMusic путь к новому файлу
     */
    DownloadMusicThread(String linksFile, String pathToMusic) {
        this.linksFile = linksFile;
        this.pathToMusic = pathToMusic;
    }

    /**
     * Запускает поток скачивания
     */
    @Override
    public void run() {
        try (BufferedReader musicFile = new BufferedReader(new FileReader(linksFile))) {
            String music;
            String name;
            int count = 0;
            try {
                while ((music = musicFile.readLine()) != null) {
                    name = pathToMusic + "music" + String.valueOf(count) + ".mp3";
                    downloadUsingNIO(music, name);
                    System.out.println("Файл " + name + " успешно скачен!");
                    count++;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод выполняющий загрузку файлов
     *
     * @param strUrl ссылка
     * @param file   файл
     * @throws IOException ошибки
     */
    private static void downloadUsingNIO(String strUrl, String file) throws IOException {
        URL url = new URL(strUrl);
        ReadableByteChannel byteChannel = Channels.newChannel(url.openStream());
        FileOutputStream stream = new FileOutputStream(file);
        stream.getChannel().transferFrom(byteChannel, 0, Long.MAX_VALUE);
        stream.close();
        byteChannel.close();
    }
}
