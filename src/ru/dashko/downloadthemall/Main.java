package ru.dashko.downloadthemall;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Класс, выполняющий скачивание музыки и картинок, а так воспроизводящий музыку
 *
 * @author Дарья "dASHKO" Шишкова, 16ИТ18к
 */
public class Main {

    private static Scanner scanner = new Scanner(System.in);

    private static final String IN_FILE_MUSIC_TXT = "src\\ru\\dashko\\downloadthemall\\inFileMusic.txt";
    private static final String IN_FILE_PICTURE_TXT = "src\\ru\\dashko\\downloadthemall\\inFilePicture.txt";
    private static final String OUT_FILE_MUSIC_TXT = "src\\ru\\dashko\\downloadthemall\\outFileMusic.txt";
    private static final String OUT_FILE_PICTURE_TXT = "src\\ru\\dashko\\downloadthemall\\outFilePicture.txt";
    private static final String PATH_TO_MUSIC = "src\\ru\\dashko\\downloadthemall\\music\\";
    private static final String PATH_TO_PICTURE = "src\\ru\\dashko\\downloadthemall\\picture\\";

    private static final String MUSIC_REGEX = "\\s*(?<=data-url\\s?=\\s?\")[^>]*\\/*(?=\")";
    private static final String PICTURE_REGEX = "\\s*(?<=img\\ssrc\\s?=\\s?\")(https?:\\/\\/)?([\\/\\w\\.-]*)*";

    public static void main(String[] args) {

        findAllLinks(IN_FILE_MUSIC_TXT, OUT_FILE_MUSIC_TXT, MUSIC_REGEX);
        findAllLinks(IN_FILE_PICTURE_TXT, OUT_FILE_PICTURE_TXT, PICTURE_REGEX);

        DownloadMusicThread downloadMusicThread = new DownloadMusicThread(OUT_FILE_MUSIC_TXT, PATH_TO_MUSIC);
        DownloadPictureThread downloadPictureThread = new DownloadPictureThread(OUT_FILE_PICTURE_TXT, PATH_TO_PICTURE);
        downloadMusicThread.start();
        downloadPictureThread.start();
        try {
            downloadMusicThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        musicPlayer();
    }

    /**
     * Музыкальный плейер, предоставляющий пользователю выбор воспроизводимой композиции
     * из скаченных нами ранее файлов
     */
    private static void musicPlayer() {
        int maxNumOfFile = 0;
        int num;
        try (Stream<Path> files = Files.list(Paths.get(PATH_TO_MUSIC))) {
            maxNumOfFile = (int) files.count() - 1;
        } catch (IOException e) {
            e.printStackTrace();
        }
        do {
            System.out.print("Введите номер скаченной композиции, которую хотите прослушать: ");
            num = scanner.nextInt();
        } while (num < 0 || num > maxNumOfFile);
        System.out.println("Наслаждайтесь...");
        playMusic(PATH_TO_MUSIC + "music" + num + ".mp3");
    }

    /**
     * Записывает в файл все ссылки на скачивание, соответствующие регулярному выражению,
     * на каждом из сайтов, записанных в тестовом файле
     *
     * @param inFile  файл, содержащий ссылки на сайты
     * @param outFile файл, куда будем записывать ссылки на скачивание
     * @param regex   регулярное выражение для поиска
     */
    private static void findAllLinks(String inFile, String outFile, String regex) {
        String Url;
        try (BufferedReader inFileReader = new BufferedReader(new FileReader(inFile))) {
            while ((Url = inFileReader.readLine()) != null) {
                URL url = new URL(Url);
                downloadLinkFinder(url, outFile, Pattern.compile(regex));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Записывает в файл, найденные на сайте по регулярке ссылки на скачивание файлов
     *
     * @param url         сайт
     * @param outFilePath путь к файлу, куда будем записывать ссылки
     * @param pattern     регулярное выражение для поиска
     * @throws IOException ошибки
     */
    private static void downloadLinkFinder(URL url, String outFilePath, Pattern pattern) throws IOException {
        try (BufferedWriter outFile = new BufferedWriter(new FileWriter(outFilePath))) {
            String result;
            try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()))) {
                result = bufferedReader.lines().collect(Collectors.joining("\n"));
            }
            Matcher matcher = pattern.matcher(result);
            int i = 0;
            while (matcher.find() && i < 2) {
                if (!matcher.group().contains("https:") && !matcher.group().contains("http:")) {
                    outFile.write(matcher.group().replaceFirst("./", url.toString()) + "\r\n");
                    i++;
                } else {
                    outFile.write(matcher.group() + "\r\n");
                    i++;
                }
            }
        }
    }

    /**
     * Воспроизводит музыку по указанному пути
     *
     * @param pathToMusic путь к музыке
     */
    private static void playMusic(String pathToMusic) {
        try (FileInputStream inputStream = new FileInputStream(pathToMusic)) {
            try {
                Player player = new Player(inputStream);
                player.play();
            } catch (JavaLayerException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}