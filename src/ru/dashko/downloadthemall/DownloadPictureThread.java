package ru.dashko.downloadthemall;

import java.io.*;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

/**
 * Поток, выполняющий скачивание картинок формата .jpg и .png
 *
 * @author Дарья "dASHKO" Шишкова, 16ИТ18к
 */
public class DownloadPictureThread extends Thread {
    private String linksFile;
    private String pathToPicture;

    /**
     * Конструктор для потока скачивания
     *
     * @param linksFile     путь к файлу с сылками
     * @param pathToPicture путь к новому файлу
     */
    DownloadPictureThread(String linksFile, String pathToPicture) {
        this.linksFile = linksFile;
        this.pathToPicture = pathToPicture;
    }

    /**
     * Запускает поток скачивания
     */
    @Override
    public void run() {
        try (BufferedReader picFile = new BufferedReader(new FileReader(linksFile))) {
            String picture;
            String name;
            int count = 0;
            try {
                while ((picture = picFile.readLine()) != null) {
                    if (picture.contains(".jpg")) {
                        name = pathToPicture + "picture" + String.valueOf(count) + ".jpg";
                        downloadUsingNIO(picture, name);
                        System.out.println("Файл " + name + " успешно скачен!");
                        count++;
                    } else if (picture.contains(".png")) {
                        name = pathToPicture + "picture" + String.valueOf(count) + ".jpg";
                        downloadUsingNIO(picture, name);
                        System.out.println("Файл " + name + " успешно скачен!");
                        count++;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод выполняющий загрузку файлов
     *
     * @param strUrl ссылка
     * @param file   файл
     * @throws IOException ошибки
     */
    private static void downloadUsingNIO(String strUrl, String file) throws IOException {
        URL url = new URL(strUrl);
        ReadableByteChannel byteChannel = Channels.newChannel(url.openStream());
        FileOutputStream stream = new FileOutputStream(file);
        stream.getChannel().transferFrom(byteChannel, 0, Long.MAX_VALUE);
        stream.close();
        byteChannel.close();
    }
}
