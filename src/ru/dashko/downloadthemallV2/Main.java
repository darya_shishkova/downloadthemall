package ru.dashko.downloadthemallV2;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

import java.io.*;

/**
 * Класс, выполняющий скачивание музыки и картинок, а так воспроизводящий музыку
 *
 * @author Дарья "dASHKO" Шишкова, 16ИТ18к
 */
public class Main {
    private static final String IN_FILE_MUSIC_TXT = "src\\ru\\dashko\\downloadthemallV2\\inFileMusic.txt";
    private static final String IN_FILE_PICTURE_TXT = "src\\ru\\dashko\\downloadthemallV2\\inFilePicture.txt";

    public static void main(String[] args) {
        DownloadMusicThread downloadMusicThread = new DownloadMusicThread(IN_FILE_MUSIC_TXT);
        DownloadPictureThread downloadPictureThread = new DownloadPictureThread(IN_FILE_PICTURE_TXT);
        downloadMusicThread.start();
        downloadPictureThread.start();
        try {
            downloadMusicThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Все файлы успешно скачены! А теперь наслаждайтесь...");
        playAll();
    }

    /**
     * Воспроизводит все треки, путь к которым указан в файле
     */
    private static void playAll() {
        try (BufferedReader musicFile = new BufferedReader(new FileReader(IN_FILE_MUSIC_TXT))) {
            String music;
            String name;
            int count = 0;
            while ((music = musicFile.readLine()) != null) {
                String[] linkAndPath = music.split(" ");
                name = linkAndPath[1] + String.valueOf(count) + ".mp3";
                playMusic(name);
                count++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Воспроизводит музыку по указанному пути
     *
     * @param pathToMusic путь к музыке
     */
    private static void playMusic(String pathToMusic) {
        try (FileInputStream inputStream = new FileInputStream(pathToMusic)) {
            try {
                Player player = new Player(inputStream);
                player.play();
            } catch (JavaLayerException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}