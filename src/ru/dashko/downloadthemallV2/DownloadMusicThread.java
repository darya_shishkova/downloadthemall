package ru.dashko.downloadthemallV2;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

/**
 * Поток, выполняющий скачивание музыкальных файлов
 *
 * @author Дарья "dASHKO" Шишкова, 16ИТ18к
 */
public class DownloadMusicThread extends Thread {

    private String source;

    /**
     * Конструктор для потока скачивания
     *
     * @param source     путь к файлу с cсылками
     */
    DownloadMusicThread(String source) {
        this.source = source;
    }

    /**
     * Запускает поток скачивания
     */
    @Override
    public void run() {
        try (BufferedReader musicFile = new BufferedReader(new FileReader(source))) {
            String music;
            String name;
            int count = 0;
            try {
                while ((music = musicFile.readLine()) != null) {
                    String[] linkAndPath = music.split(" ");
                    name = linkAndPath[1] + String.valueOf(count) + ".mp3";
                    downloadUsingNIO(linkAndPath[0], name);
                    System.out.println("Файл " + name + " успешно скачен!");
                    count++;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод выполняющий загрузку файлов
     * @param strUrl ссылка
     * @param file файл
     * @throws IOException ошибки
     */
    private static void downloadUsingNIO(String strUrl, String file) throws IOException {
        URL url = new URL(strUrl);
        ReadableByteChannel byteChannel = Channels.newChannel(url.openStream());
        FileOutputStream stream = new FileOutputStream(file);
        stream.getChannel().transferFrom(byteChannel, 0, Long.MAX_VALUE);
        stream.close();
        byteChannel.close();
    }
}
