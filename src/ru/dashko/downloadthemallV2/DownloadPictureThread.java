package ru.dashko.downloadthemallV2;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

/**
 * Поток, выполняющий скачивание картинок формата .jpg и .png
 *
 * @author Дарья "dASHKO" Шишкова, 16ИТ18к
 */
public class DownloadPictureThread extends Thread {

    private String source;

    /**
     * Конструктор для потока скачивания
     *
     * @param source     путь к файлу с cсылками
     */
    DownloadPictureThread(String source) {
        this.source = source;
    }

    /**
     * Запускает поток скачивания
     */
    @Override
    public void run() {
        try (BufferedReader picFile = new BufferedReader(new FileReader(source))) {
            String picture;
            String name;
            int count = 0;
            try {
                while ((picture = picFile.readLine()) != null) {
                    String[] linkAndPath = picture.split(" ");
                    if (picture.contains(".jpg")) {
                        name = linkAndPath[1] + String.valueOf(count) + ".jpg";
                        downloadUsingNIO(linkAndPath[0], name);
                        System.out.println("Файл " + name + " успешно скачен!");
                        count++;
                    } else if (picture.contains(".png")) {
                        name = linkAndPath[1] + String.valueOf(count) + ".jpg";
                        downloadUsingNIO(linkAndPath[0], name);
                        System.out.println("Файл " + name + " успешно скачен!");
                        count++;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод выполняющий загрузку файлов
     *
     * @param strUrl ссылка
     * @param file   файл
     * @throws IOException ошибки
     */
    private static void downloadUsingNIO(String strUrl, String file) throws IOException {
        URL url = new URL(strUrl);
        ReadableByteChannel byteChannel = Channels.newChannel(url.openStream());
        FileOutputStream stream = new FileOutputStream(file);
        stream.getChannel().transferFrom(byteChannel, 0, Long.MAX_VALUE);
        stream.close();
        byteChannel.close();
    }
}
